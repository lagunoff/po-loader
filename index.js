/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author David Schissler @dschissler
*/
var po2json = require('po2json');
var loaderUtils = require('loader-utils');
var path = require('path');

module.exports = function(source) {
	this.cacheable();

	var options = loaderUtils.parseQuery(this.query);

	// default option
	if (!('stringify' in options)) {
		options.stringify = true;
	}

        if ('useBasenameAsDomain' in options) {
                if (options.useBasenameAsDomain) {
                        var chunks = path.parse(this.resourcePath);
                        options.domain = chunks.name;
                 }
                 delete options.useBasenameAsDomain;
        }

	jsonData = po2json.parse(source, options);

	return jsonData;
}
